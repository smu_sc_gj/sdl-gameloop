/*
  Based on http://lazyfoo.net/SDL_tutorials/
  Used with permission. 
*/

//Include SDL library
//This is now cross platform. 
#if defined(_WIN32)
    #include "SDL.h"
#else
    #include "SDL/SDL.h"
#endif

const int FRAME_RATE=16; //our desired frame rate
                         //approx. (1/60)*1000 - 60fps.

int main( int argc, char* args[] )
{
    //Event driven programming variables
    SDL_Event event; //to store the event from SDL
    int quit = 1;  //keep the game loop running (starts at false).

    //timer variables
    float scalar;
    Uint32 startTime;
    Uint32 currentTime;
    Uint32 delta;

    //Declare pointers to surface structures
    SDL_Surface* screen = NULL;

    //Initialise SDL
    //NOTE: SDL_INIT_EVERYTHING is a constant defined by SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Surface structure we'll use for the window - screen
    //This is essentially the back buffer. 
    screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE );


    //Initlaise the timer
    startTime = SDL_GetTicks();

    //Event loop
    while(quit == 1)
    {
        //If there's events to handle 
        if( SDL_PollEvent( &event ) ) 
        { 
            switch(event.type) 
            { 
                case SDL_QUIT: //If the user has closed(x) out the window 
                    //Quit the program 
                    quit = 0;
                break; 
                case SDL_MOUSEBUTTONDOWN: //If the left mouse button was pressed 
                     if( event.button.button == SDL_BUTTON_LEFT )  
                         quit = 0;
                break;    
		 
                 case SDL_KEYDOWN: //If the escape key is pressed
                     if(event.key.keysym.sym == SDLK_ESCAPE)
                         quit = 0;
                 break;
            }
        }

        //Check time - work out scalar
        currentTime = SDL_GetTicks();
        Uint32 delta = currentTime - startTime;

        if(delta > FRAME_RATE)
        {
           // Reset for next time
           startTime = currentTime;

           //Calculate scalar - will be 1 if we're dead on the 
           //frame rate or some fraction of if we're over/under
           scalar = delta / (FRAME_RATE*1.0f);

           //update sprites use scalar (time delta in unity) to smooth animation
        
           //draw sprites (e.g. player)

           //Refresh the screen (replace with backbuffer). 
           SDL_Flip( screen );
        }
        else
        {
            //A delay of zero is enough to put the process back into 
            //the ready queue.
            SDL_Delay(0);   
        }
    }

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
